/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 *
 * @author Nacho C
 * @param <E>
 * como utilizaremos los elementos de cada lista definidos en la lista Doblemente
 * ligada extendiendolos a un caso generico
 */
public interface Lista<E> {
    void agregar(E elemento);
    void agregar(int indice, E elemento);
    E eliminar(int indice);
    void eliminar(E elemento);
    int indiceDe(E elemento);
    E get(int indice);
    boolean isVacia();
    int tamanio(); 
}
