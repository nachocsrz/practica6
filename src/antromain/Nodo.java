/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 * Nodo.java
 * Clase que genera objetos de tipo Nodo, los cuales son elementos de un arbol
 * binario. 
 * @author Castillo Suarez Ignacio, Lopez Velazco Daniel, Martinez Salinas
 * Jose Antonio
 * @param <E> Tipo de dato generico del nodo
 * @version Nov 2019
 */
public class Nodo<E> {
    private Nodo<E> izquierdo;
    private E elemento;
    private Nodo<E> derecho;

     /**
      * Constructor por parametros que recibe un objeto de tipo E y genara
      * un nodo para el arbol con el elemento recibido
      * @param elemento Elemento contenido en el nodo creado
      */
    
    public Nodo(E elemento) {
        this.elemento = elemento;
    }
    
   /**
     * Constructor por parametros que, ademas de recibir el elemento del nodo,
     * recibe los hijos izquierdo y derecho del mismo nodo
     * @param izquierdo Hijo izquierdo del nodo creado
     * @param derecho Hijo izquierdo del nodo creado
     * @param elemento Elemento contenido en el nodo creado
     */
    public Nodo(Nodo<E> izquierdo, E elemento, Nodo<E> derecho) {
        this.izquierdo = izquierdo;
        this.elemento = elemento;
        this.derecho = derecho;
    }

    /**
     * Getter del hijo derecho de un nodo
     * @return El hijo derecho del nodo
     */
    public Nodo<E> getDerecho() {
        return derecho;
    }
    
    /**
     * Setter del hijo derecho de un nodo
     * @param derecho Nodo a establecer como hijo derecho
     */
    public void setDerecho(Nodo<E> derecho) {
        this.derecho = derecho;
    }

    /**
     * Getter del hijo izquierdo de un nodo
     * @return El hijo izquierdo del nodo
     */
    public Nodo<E> getIzquierdo() {
        return izquierdo;
    }
    /**
     * Setter del hijo izquierdo de un nodo
     * @param izquierdo Nodo a establecer como hijo izquierdo
     */
    public void setIzquierdo(Nodo<E> izquierdo) {
        this.izquierdo = izquierdo;
    }

    /**
     * Getter del elemento contenido en un nodo
     * @return El elemento del nodo
     */
    public E getElemento() {
        return elemento;
    }

    /**
     * Setter del elemento dentro de un nodo
     * @param elemento Elemento a establecer en el nodo
     */
    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    /**
     * Metodo para la representacion del nodo al usuario
     * @return Cadena con la forma del nodo
     */
    @Override
    public String toString() {
        return elemento.toString();
    }
}

