/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 * ColaDinamica.java
 * Clase que determina las Colas Dinamicas, apoyandose de la interfaz de 
 * colas, que requiere de datos genericos de tipo E. Se basa en la Lista
 * Doblemente Ligada
 * @author Castillo Suarez Ignacio, Lopez Velazco Daniel, Martinez Salinas 
 * Jose Antonio
 * @version Nov 19
 * @param <E> Tipo de dato admitido por la Cola
 */
public class ColaDinamica<E> implements Cola<E> {
    private Lista<E> elementos;

    /**
     * Constructor por omision que inicializa la lista como vacia
     */
    public ColaDinamica() {
        this.elementos = new ListaDoblementeLigada<>();
    }

    /**
     * Metodo que regresa la lista de elementos de la cola
     * @return La lista de elementos de la cola
     */
    public Lista<E> getElementos() {
        return elementos;
    }
    
    /**
     * Metodo que inserta un elemento dado a la cola
     * @param elemento El dato a insertar
     */
    
    @Override
    /*
    En este metodo insertar llamamos la representacion que tiene en la Lista, y 
    este a su vez es agregar los elementos al final,como lo definimos en la lista
    */
    public void insertar(E elemento) {
        elementos.agregar(elemento);
    }

    /**
     * Metodo que permite eliminar un elemento de la cola
     * @return Elemento que fue removido
     */
    @Override
    
    /*
    En el metoco remover utilizamos que removera el primer elementos que se encuentre
    en la lista, en este caso sera el elemento 1 por como lo definimos 
    */
    public E remover() {
        return elementos.eliminar(0);
    }

    /**
     * Metodo que genera la representacion grafica de la cola
     * @return La representacion de la cola
     */
    @Override
    public String toString() {
        return elementos.toString();
    }
    
}