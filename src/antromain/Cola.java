/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 *
 * @author Nacho C
 * @param <E>
 * clase en la cual se desarrollara la interface tomando 
 * la cola dinamica con sus respectivas especificaciones
 */
public interface Cola<E> {
    void insertar(E elemento);
    E remover();
}