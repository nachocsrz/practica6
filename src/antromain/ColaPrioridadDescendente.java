/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 * ColaPrioridadDescendente.java
 * Clase que determina un tipo de cola de prioridad descendente, modifica el
 * metodo remover de la clase padre para implementar el comparable
 * @author Castillo Suarez Ignacio, Lopez Velazco Daniel, Martinez Salinas
 * Jose Antonio
 * @param <E> Tipo de dato de la cola
 * @version Nov 19
 */
public class ColaPrioridadDescendente<E extends Comparable<E>> 
       extends ColaDinamica<E>{

    /**
     * Metodo sobreescrito que elimina elementos de la cola, haciendo uso
     * del metodo compareTo de los objetos ingresados a la cola
     * @return El elemento eliminado de la cola
     */
    @Override
    public E remover() {
        E aux = this.getElementos().get(0);
        for(int i = 0; i<this.getElementos().tamanio(); i++){
            if(aux.compareTo(getElementos().get(i))<0)
                aux=getElementos().get(i);
        }
        
        return getElementos().eliminar(getElementos().indiceDe(aux));
    }
    
}
