/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

/**
 * ListaDoblementeLigada.java
 * Clase que genera objetos de tipo listas doblemente ligadas, una estructura
 * de datos que se apoya de los Nodos, similares a los de los arboles binarios
 * de busqueda
 * @author Castillo Suarez Ignacio, Lopez Velazco Daniel, Martinez Salinas
 * Jose Antonio
 * @param <E> Tipo de dato de la lista
 * @version Nov 19
 */
public class ListaDoblementeLigada<E> implements Lista<E>{
    private Nodo<E> inicio;
    private Nodo<E> fin;
    private int tamanio;

    /**
     * Metodo que valida los indices solicitados para los metodos
     * @param indice Indice a validar
     */
    private void validarIndice(int indice) {
        if(isVacia() || (indice < 0 || indice >= tamanio))
            throw new IndexOutOfBoundsException();
    }
    /**
     * Metodo para agregar un elemento a la lista. Determina primero si la 
     * lista esta vacia, de ser asi agrega el elemento en la primera posicion;
     * de no serlo, lo arega en la posicion requerida y aumenta el tamanio de la
     * lista en una unidad
     * @param elemento El elemento a agregar a la lista
     */
    @Override
    public void agregar(E elemento) {
        Nodo<E> nuevo = new Nodo<>(elemento);
        
        if (isVacia()) {
            inicio = fin = nuevo;
        }
        else {
            fin.setDerecho(nuevo);
            nuevo.setIzquierdo(fin);
            fin = nuevo;
        }
        
        tamanio++;
    }

    @Override
    public void agregar(int indice, E elemento) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Metodo para eliminar un elemento de la lista dado un indice deseado
     * @param indice Indice del elemento a eliminar
     * @return Informacion del elemento eliminado
     */
    @Override
    public E eliminar(int indice) {
        validarIndice(indice);
        
        E elemento = null;
        if(tamanio == 1) {
            elemento = inicio.getElemento();
            inicio = fin = null;
        } else if(indice == 0) {
            elemento = inicio.getElemento();
            inicio = inicio.getDerecho();
            inicio.setIzquierdo(null);
        } else if(indice == tamanio-1) {
            elemento = fin.getElemento();
            fin = fin.getIzquierdo();
            fin.setDerecho(null);
        } else {
            Nodo<E> auxiliar = inicio;
            for (int i = 0; i < indice; i++) {
                auxiliar = auxiliar.getDerecho();
            }
            
            elemento = auxiliar.getElemento();
            auxiliar.getDerecho().setIzquierdo(auxiliar.getIzquierdo());
            auxiliar.getIzquierdo().setDerecho(auxiliar.getDerecho());
        }
        
        tamanio--;
        return elemento;
    }

    /**
     * Metodo para buscar un elemento en la lista para su posterior eliminacion
     * @param elemento Elemento que se desea eliminar.
     */
    @Override
    public void eliminar(E elemento) {
        int indice = indiceDe(elemento);
        
        if (indice != -1) {
            eliminar(indice);
        }
    }

    /**
     * Metodo para encontrar el indice de un elemento buscado
     * @param elemento El elemento a buscar en la lista
     * @return El indice del elemento buscado
     */
    @Override
    public int indiceDe(E elemento) {
        int indice = 0;
        
        for (Nodo<E> auxiliar = inicio; 
            auxiliar != null; 
            auxiliar = auxiliar.getDerecho(),indice++) {
            if(auxiliar.getElemento().equals(elemento))
                return indice;
        }
        
        return -1;
    }

    /**
     * Metodo para encontrar el elemento de la lista dado un indice
     * @param indice El indice en el que se desea buscar
     * @return El elemento correspondiente al indice ingresado
     */
    @Override
    public E get(int indice) {
        validarIndice(indice);
        
        Nodo<E> auxiliar = inicio;
        for (int i = 0; i < indice; i++) {
            auxiliar = auxiliar.getDerecho();
        }
        
        return auxiliar.getElemento();
    }

    /**
     * Metodo que determina si la lista se encuentra vacia
     * @return Si la lista esta vacia
     */
    @Override
    public boolean isVacia() {
        return inicio == null;
    }

    /**
     * Metodo que regresa el numero de elementos de la lista
     * @return El numero de elementos de la lista
     */
    @Override
    public int tamanio() {
        return tamanio;
    }

    /**
     * Metodo para generar la representacion grafica de la lista
     * @return Representacion de la lista
     */
    @Override
    public String toString() {
        String representacion = "";
        
        for (Nodo<E> auxiliar = inicio; auxiliar != null; 
                auxiliar = auxiliar.getDerecho()) {
            representacion += auxiliar + " -> ";
        }
        
        return representacion;
    }
    
}
