/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antromain;

import java.util.Random;

/**
 * Persona.java
 * Clase que genera instancias de tipo Persona, con sexo , economia y nombre, 
 * que implementan la interfaz comparable. Se apoya de metodos aleatorios
 * para determinar los nombres de un arreglo.
 * @author Castillo Suarez Ignacio, Lopez Velazco Daniel, Martinez Salinas
 * Jose Antonio
 * @version Nov 19
 */
public class Persona implements Comparable<Persona> {
    private String nombre;
    private double dinero;
    private char sexo;
    
     Random variable = new Random();
     

    String [] nombreH = { "Jose", "Andres", "Eduardo", "Antonio", "Maximino", 
        "Julian", "Juan", "Andres", "Samuel"};
    String [] nombreM = {"Andrea", "Julieta", "Karen", "Cecilia", "Ximena", 
        "Gloria", "Martha", "Rosa", "Maria", "Guadalupe"};
    
   /**
    * Constructor por parametros que recibe nombre, sexo y economia de una 
    * persona
    * @param n Nombre de la persona
    * @param s Sexo de la persona
    * @param d Economia de la persona
    */
    public Persona(String n, char s, double d){
        nombre = n;
        setSexo(s);
        dinero = d;
    }
    
    /**
     * Constructor por omision
     */
    public Persona(){
    
    }
    
    /**
     * Getter para el nombre de la persona
     * @return Nombre de la persona
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Setter del nombre de la persona
     * @param nombre Nombre a elegir de la persona
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Setter del nombre de la persona; se apoya de metodos aleatorios para
     * determinarlo
     * @return El nombre elegido
     */
    public String setNombre(){
      if(variable.nextInt(2)==0){
          return nombreH[(int) Math.floor(Math.random()*((nombreH.length-1)-0+1))];
      }
      else{
        return nombreM[(int) Math.floor(Math.random()*((nombreM.length-1)-0+1))];
      }
    }
    
    /**
     * Getter del dinero de la persona
     * @return El dinero de la persona
     */
    public double getDinero() {
        return dinero;
    }

    /**
     * Setter del dinero de la persona
     * @param dinero El dinero a otorgar a la persona
     */
    public void setDinero(double dinero) {
        this.dinero = dinero;
    }
    
    /**
     * Setter del dinero de la persona que tiene un tope al 100000
     * @return El dinero de la persona
     */
    public double setDinero(){
       return Math.floor(Math.random()*100000+1);
    }

    /**
     * Getter del sexo de la persona
     * @return El sexo de la persona
     */
    public char getSexo() {
        return sexo;
    }
    /**
     * Setter del sexo de la persona
     * @param s El sexo para la persona
     */
    private void setSexo(char s) {
        char sexo = Character.toUpperCase(s);
        this.sexo = sexo;
    }
    
    /**
     * Metodo compareTo, que primero coloca a las mujeres, y luego a las
     * personas con mayor cantidad de dinero
     * @param p La persona a comparar
     * @return Un entero positivo si es mayor, negativo si es menor o 0 si es 
     * igual
     */
    @Override
    public int compareTo(Persona p){
        if(this.sexo == 'F' && p.getSexo() != 'F')
            return 1;
        else if(sexo != 'F' && p.getSexo() == 'F')
            return -1;
        else{
            if(this.dinero > p.getDinero())
                return 1;
            else if(this.dinero < p.getDinero())
                return -1;
            else
                return 0;
        }
    }
    
    /**
     * Metodo que regresa la representacion grafica de la persona
     * @return Representacion grafica de la persona
     */
    @Override
    public String toString(){
        return nombre+"/"+sexo+"/"+dinero;
    }
}
